#ifndef _RestWriter_h
#define _RestWriter_h

//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C++
#include <iostream>

// POCO
#include <Poco/URI.h>

#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>

// External
#include <streamlog/streamlog.h>

// Internal
#include "Eloquent/Extensions/IOExtension.h"

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// RestWriter : IOExtension
	///////////////////////////////////////////////////////////////////////////////
	class RestWriter : public IOExtension {
		RestWriter(); 
		
	public:
		explicit RestWriter( const boost::property_tree::ptree::value_type& i_Config
							, std::mutex& i_LogMutex
							, streamlog::severity_log& i_Log
							, std::mutex& i_QueueMutex
							, std::condition_variable& i_QueueCV
							, std::queue<QueueItem>& i_Queue
							, int& i_NumWriters );

		virtual ~RestWriter();
		virtual void operator()();
		
		void Write( const std::string& ); 

	private:
		// Networking
		Poco::URI* m_URI;
		Poco::Net::HTTPClientSession* m_Session;
		Poco::Net::HTTPRequest* m_Request;

	};
}

#endif // _RestWriter_h
